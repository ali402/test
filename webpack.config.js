var webpack = require('webpack');

module.exports = {
    devtool: 'inline-source-map',
    entry: './resources/assets/js/main.js',
    output: {
        filename: 'dev.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                options: {
                  presets: [
                    'env'
                  ]
                }
            }
        ]
    },

    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false}),
    ]
};
